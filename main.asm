%define BUFF 255
%define SIZE 8



section .rodata
msg: db "No such word", 10, 0  
msg2: db "read error", 10, 0
%include "words.inc"

section .text

%include "lib.inc"

global _start

_start:
	push rbp;
    mov rbp, rsp
	sub rsp, BUFF				; allocate buffer
	mov rdi, rsp				; rdi <- buffer address
	mov rsi, BUFF				; rsi <- buffer size
	push rdi
	push rsi
	call read_word_fix			; read word to buffer
	pop rsi
	pop rdi
	cmp rax, 0					; word is bigger then buffer?
	je .overflow				; 	then overflow
	mov rdi, rax				; rdi <- word address 
	mov rsi, last_elem			; rsi <- dictionary start address
	push rdi
	push rsi 
	call find_word				; searching for a word
	pop rsi
	pop rdi
    push rax
    push rdi
    push rsi
    call print_newline
    pop rsi
    pop rdi
    pop rax
	cmp rax, 0					; not found
	je .not_found
	add rax, SIZE				; skip label
	push r12 
	mov r12, rax
	mov rdi, rax
	push rdi
	push rsi
	call string_length			; key length
	pop rsi
	pop rdi
    inc rax
	add rax, r12				; skip both key and label
	pop r12
	mov rdi, rax
	call print_string
	mov rsp, rbp
	pop rbp
	mov rdi, EXIT_CODE_OK
    jmp exit
.not_found:
	mov rdi, msg
	call error_found
    jmp .end
.overflow:
    mov rdi, msg2
	call error_found
    jmp .end
.end:
    mov rsp, rbp
    pop rbp
    mov rdi, EXIT_CODE_ERROR
    call exit
