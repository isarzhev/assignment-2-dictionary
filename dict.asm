section .text
extern string_equals
global find_word

; Pointer to null-terminated string
; Pointer to dictionary start
; If found, returns pointer to the beginning of entry, otherwise returns 0

find_word:
	xor rax, rax 			; rax <- 0

.loop: 
	cmp rsi, 0				; dictionary empty? 
	je .end					; 	if so return
	push rsi
	push rdi
	add rsi, 8				; compare key to string
	call string_equals
	pop rdi
	pop rsi
	cmp rax, 1				; found entry
	je .found
	mov rsi, [rsi]			; not this element, next
	jmp .loop

.end:
	xor rax, rax ;
	ret

.found:
	mov rax, rsi 
	ret
