ASM = nasm
ASM_FLAGS = -f elf64
LD = ld

all: main

%.o: %.asm
	nasm  -o $@ $(ASM_FLAGS) $^

main: main.o lib.o dict.o 
	ld -o $@ $^

clean:
	rm -rf *.o main 

.PHONY: all clean
